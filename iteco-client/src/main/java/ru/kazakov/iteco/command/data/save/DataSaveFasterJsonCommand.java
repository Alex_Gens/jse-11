package ru.kazakov.iteco.command.data.save;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.endpoint.Session;

@NoArgsConstructor
public final class DataSaveFasterJsonCommand extends DataAbstractSaveCommand {

    @Getter
    @NotNull
    private final String name = "data-save-faster-json";

    @Getter
    @NotNull
    private final String description = "Save data in json by FasterXml.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (domainEndpoint == null) throw new Exception();
        @Nullable final Session userSession = currentState.getUserSession();
        if (userSession == null) throw new Exception();
        if (!confirmed()) return;
        @NotNull final String fileName = "faster.json";
        domainEndpoint.saveDomainFasterJson(userSession, directory, fileName);
        terminalService.write("[SAVED]");
        terminalService.write("Data successfully saved in " + fileName + "!");
        terminalService.separateLines();
    }

}
