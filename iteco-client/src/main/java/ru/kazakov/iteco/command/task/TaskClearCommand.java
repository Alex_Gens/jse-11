package ru.kazakov.iteco.command.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.endpoint.Session;
import ru.kazakov.iteco.api.endpoint.Task;

@NoArgsConstructor
public final class TaskClearCommand extends TaskAbstractCommand {

    @Getter
    @NotNull
    private final String name = "task-clear";

    @Getter
    @NotNull
    private final String description = "Remove all tasks.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (taskEndpoint == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final Session userSession = currentState.getUserSession();
        if (userSession == null) throw new Exception();
        @Nullable final Task task = getTaskByPart();
        if (task == null) return;
        taskEndpoint.removeAllTasksByCurrentId(userSession);
        terminalService.write("[ALL TASKS REMOVED]");
        terminalService.write("Tasks successfully removed!");
        terminalService.separateLines();
    }

}
