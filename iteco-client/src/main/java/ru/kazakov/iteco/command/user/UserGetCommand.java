package ru.kazakov.iteco.command.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.endpoint.RoleType;
import ru.kazakov.iteco.api.endpoint.Session;
import ru.kazakov.iteco.api.endpoint.User;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.SimpleDateFormat;
import java.util.Date;

@NoArgsConstructor
public final class UserGetCommand extends UserAbstractCommand {

    @Getter
    @NotNull
    private final String name = "user-get";

    @Getter
    @NotNull
    private final String description = "Show user profile information.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (userEndpoint == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final Session userSession = currentState.getUserSession();
        if (userSession == null) throw new Exception();
        @Nullable final RoleType userSessionRoleType = userSession.getRoleType();
        if (userSessionRoleType == null) throw new Exception();
        @Nullable User user = null;
        if (userSessionRoleType == RoleType.ADMINISTRATOR) {
            terminalService.write("Enter user's login to change profile password.   [" +
                    userSessionRoleType + "]");
            terminalService.write("ENTER LOGIN: ");
            @NotNull final String login = terminalService.enterIgnoreEmpty();
            boolean isExist = userEndpoint.containsUser(login);
            if (!isExist) {
                terminalService.write("[NOT CORRECT]");
                terminalService.write("User with that login doesn't exist.");
                terminalService.separateLines();
                return;
            }
            user = userEndpoint.findUserByLogin(userSession, login);
        }
        if (user == null) throw new Exception();
        terminalService.write("Profile information: ");
        @Nullable final String name = user.getName();
        boolean nameIsExist = name != null && !name.isEmpty();
        if (nameIsExist) terminalService.write("Name: " + name);
        @Nullable final String login = user.getLogin();
        final boolean loginIsExist = login != null && !login.isEmpty();
        if (loginIsExist) terminalService.write("Login: " + login);
        if (!nameIsExist && !loginIsExist) {
            terminalService.write("Profile has no information.");
            terminalService.separateLines();
            return;
        }
        @Nullable final XMLGregorianCalendar gregory = user.getDateStart();
        if (gregory == null) return;
        @NotNull final Date dateStart = gregory.toGregorianCalendar().getTime();
        @NotNull final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        terminalService.write("Registration date: " + dateFormat.format(dateStart));
        terminalService.separateLines();

    }

}
