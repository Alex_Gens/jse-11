package ru.kazakov.iteco.command.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.endpoint.ISessionEndpoint;
import ru.kazakov.iteco.api.endpoint.Session;

@NoArgsConstructor
public final class UserLogoutCommand extends UserAbstractCommand {

    @Getter
    @NotNull
    private final String name = "user-logout";

    @Getter
    @NotNull
    private final String description = "Logout from account.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        ISessionEndpoint sessionEndpoint = serviceLocator.getSessionEndpoint();
        @Nullable final Session userSession = currentState.getUserSession();
        if (userSession == null) throw new Exception();
        sessionEndpoint.removeSession(userSession.getId());
        currentState.setUserSession(null);
        terminalService.write("[LOGOUT]");
        terminalService.write("You logout from your account.");
        terminalService.separateLines();
    }

}
