package ru.kazakov.iteco.command.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.endpoint.Session;
import ru.kazakov.iteco.api.endpoint.Task;

@NoArgsConstructor
public final class TaskCreateCommand extends TaskAbstractCommand {

    @Getter
    @NotNull
    private final String name = "task-create";

    @Getter
    @NotNull
    private final String description = "Create new task.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (taskEndpoint == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final Session userSession = currentState.getUserSession();
        if (userSession == null) throw new Exception();
        terminalService.write("ENTER TASK NAME: ");
        @NotNull final String name = terminalService.enterIgnoreEmpty().trim();
        if (taskEndpoint.containsTaskByCurrentId(userSession, name)) {
            terminalService.write("[NOT CREATED]");
            terminalService.write("Task with this name is already exists. Use another task name.");
            terminalService.separateLines();
            return;
        }
        @NotNull final Task task = new Task();
        task.setUserId(userSession.getUserId());
        task.setName(name);
        taskEndpoint.persistTask(userSession, task);
        terminalService.write("[CREATED]");
        terminalService.write("Task successfully created!");
        terminalService.separateLines();
    }

}
