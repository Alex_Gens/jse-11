package ru.kazakov.iteco.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.endpoint.*;
import ru.kazakov.iteco.api.repository.IProjectRepository;
import ru.kazakov.iteco.api.repository.ISessionRepository;
import ru.kazakov.iteco.api.repository.ITaskRepository;
import ru.kazakov.iteco.api.repository.IUserRepository;
import ru.kazakov.iteco.api.service.*;
import ru.kazakov.iteco.endpoint.*;
import ru.kazakov.iteco.repository.ProjectRepository;
import ru.kazakov.iteco.repository.SessionRepository;
import ru.kazakov.iteco.repository.TaskRepository;
import ru.kazakov.iteco.repository.UserRepository;
import ru.kazakov.iteco.service.*;
import javax.xml.ws.Endpoint;

@NoArgsConstructor
public class Bootstrap implements ServiceLocator {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(projectService, taskService, userService, sessionService);

    @NotNull
    private final ServiceLocator serviceLocator = this;

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(serviceLocator);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(serviceLocator);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(serviceLocator);

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(serviceLocator);

    public void start(){
        Endpoint.publish("http://" + propertyService.getHost() + ":"
                                          + propertyService.getPort()
                                          + "/ProjectEndpoint?wsdl", projectEndpoint);
        Endpoint.publish("http://" + propertyService.getHost() + ":"
                                          + propertyService.getPort()
                                          + "/TaskEndpoint?wsdl", taskEndpoint);
        Endpoint.publish("http://" + propertyService.getHost() + ":"
                                          + propertyService.getPort()
                                          + "/UserEndpoint?wsdl", userEndpoint);
        Endpoint.publish("http://" + propertyService.getHost() + ":"
                                          + propertyService.getPort()
                                          + "/SessionEndpoint?wsdl", sessionEndpoint);
        Endpoint.publish("http://" + propertyService.getHost() + ":"
                                          + propertyService.getPort()
                                          + "/DomainEndpoint?wsdl", domainEndpoint);
    }

}
