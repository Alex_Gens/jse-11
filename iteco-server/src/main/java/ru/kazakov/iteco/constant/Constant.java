package ru.kazakov.iteco.constant;

import org.jetbrains.annotations.NotNull;

public class Constant {

    @NotNull
    public static final String SALT = "@JF27$o%";

    public static final int CYCLE = 60000;

    public static final long SESSION_TIME = 1800000;

}
