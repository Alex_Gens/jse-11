package ru.kazakov.iteco.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.repository.ISessionRepository;
import ru.kazakov.iteco.api.service.ISessionService;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.entity.Session;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.util.Password;
import ru.kazakov.iteco.util.SignatureUtil;

public class SessionService extends AbstractService<Session, ISessionRepository> implements ISessionService {

    @NotNull
    private final ISessionRepository repository;

    public SessionService(@NotNull final ISessionRepository repository) {this.repository = repository;}

    @Nullable
    @Override
    public Session getInstance(@Nullable final String login,
                               @Nullable final String password,
                               @NotNull final ServiceLocator serviceLocator) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception();
        if (password == null || password.isEmpty()) throw new Exception();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new Exception();
        if (user.getPassword() == null || user.getPassword().isEmpty()) throw new Exception();
        @NotNull final String enteredPassword = Password.getHashedPassword(password);
        if (user.getPassword().equals(enteredPassword)) {
            Session userSession = new Session();
            userSession.setRoleType(user.getRoleType());
            userSession.setUserId(user.getId());
            @Nullable final String signature = SignatureUtil.getSignature(userSession);
            if (signature == null || signature.isEmpty()) throw new Exception();
            userSession.setSignature(signature);
            return userSession;
        }
        return null;
    }

    @Override
    public boolean contains(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (id == null || id.isEmpty()) throw new Exception();
        return repository.contains(userId, id);
    }

    @NotNull
    @Override
    protected ISessionRepository getRepository() {return repository;}

}
