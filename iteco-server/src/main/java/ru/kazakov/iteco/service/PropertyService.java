package ru.kazakov.iteco.service;

import org.jetbrains.annotations.NotNull;
import ru.kazakov.iteco.api.service.IPropertyService;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private final Properties properties = new Properties();



    public PropertyService() {
        try(InputStream is =  PropertyService.class.getClassLoader()
                .getResourceAsStream("config.properties")) {
            properties.load(is);
        } catch (IOException e) {e.getMessage();}
    }

    @NotNull
    @Override
    public String getHost() {
        return properties.getProperty("host");

    }

    @NotNull
    @Override
    public String getPort() {
        return properties.getProperty("port");
    }

    @NotNull
    @Override
    public String getSalt() {
        return properties.getProperty("salt");
    }

    @NotNull
    @Override
    public String getCycle() {
        return properties.getProperty("cycle");
    }

    @NotNull
    @Override
    public String getSessionTime() {
        return properties.getProperty("sessionTime");
    }

}
