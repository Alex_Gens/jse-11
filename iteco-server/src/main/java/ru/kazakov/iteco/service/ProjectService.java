package ru.kazakov.iteco.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.repository.IProjectRepository;
import ru.kazakov.iteco.api.service.IProjectService;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.enumeration.SortType;
import java.util.List;

public class ProjectService extends AbstractService<Project, IProjectRepository> implements IProjectService {

    @NotNull
    private final IProjectRepository repository;

    public ProjectService(@NotNull final IProjectRepository projectRepository) {this.repository = projectRepository;}

    @Nullable
    @Override
    public String getName(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        return repository.getName(id);
    }

    @Override
    public void remove(@Nullable final List<String> ids) throws Exception {
        if (ids == null) throw new Exception();
        repository.remove(ids);
    }

    @Override
    public void removeAll(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        repository.removeAll(currentUserId);
    }

    @Nullable
    @Override
    public Project findByName(@Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        return repository.findByName(name);
    }

    @Nullable
    @Override
    public Project findByName(@Nullable final String currentUserId, @Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        return repository.findByName(currentUserId, name);
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final List<String> ids) throws Exception {
        if (ids == null) throw new Exception();
        return repository.findAll(ids);
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        return repository.findAll(currentUserId);
    }

    @Nullable
    @Override
    public List<String> findAll(@Nullable final String currentUserId, @Nullable final SortType sortType) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        if (sortType == null) throw new Exception();
        return repository.findAll(currentUserId, sortType);
    }

    @Nullable
    @Override
    public List<String> findAllByName(@Nullable final String currentUserId, @Nullable final String part) throws Exception {
        if (part == null || part.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        return repository.findAllByName(currentUserId, part);
    }

    @Nullable
    @Override
    public List<String> findAllByInfo(@Nullable final String currentUserId, @Nullable final String part) throws Exception {
        if (part == null || part.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        return repository.findAllByInfo(currentUserId, part);
    }

    @Override
    public boolean contains(@Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        return repository.contains(name);
    }

    @Override
    public boolean contains(@Nullable final String currentUserId, @Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        return repository.contains(currentUserId, name);
    }

    @Override
    public boolean isEmpty(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        return repository.isEmpty(id);
    }

    @Override
    public boolean isEmptyRepository(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        return repository.isEmptyRepository(currentUserId);
    }

    @NotNull
    @Override
    protected IProjectRepository getRepository() {return repository;}

}
