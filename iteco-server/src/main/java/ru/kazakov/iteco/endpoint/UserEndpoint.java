package ru.kazakov.iteco.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.endpoint.IUserEndpoint;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.entity.Session;
import ru.kazakov.iteco.entity.User;
import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.kazakov.iteco.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
        this.userService = serviceLocator.getUserService();
    }

    @NotNull
    private final IUserService userService;

    @Override
    @WebMethod
    public void mergeUser(@Nullable final Session userSession, @Nullable final User entity) throws Exception {
        validate(userSession);
        userService.merge(entity);
    }

    @Override
    @WebMethod
    public void persistUser(@Nullable final User entity) throws Exception {
        userService.persist(entity);
    }

    @Override
    @WebMethod
    public void removeUser(@Nullable final Session userSession, @Nullable final String id) throws Exception {
        validate(userSession);
        userService.remove(id);
    }

    @Override
    @WebMethod
    public void removeAllUsers(@Nullable final Session userSession) throws Exception {
        validate(userSession);
        userService.removeAll();
    }

    @Nullable
    @Override
    @WebMethod
    public User findOneUser(@Nullable final Session userSession, @Nullable final String id) throws Exception {
        validate(userSession);
        return userService.findOne(id);
    }

    @Nullable
    @Override
    @WebMethod
    public User findUserByLogin(@Nullable final Session userSession, @Nullable final String login) throws Exception {
        validate(userSession);
        return userService.findByLogin(login);
    }

    @Nullable
    @Override
    @WebMethod
    public List<String> findAllUsers(@Nullable final Session userSession) throws Exception {
        validate(userSession);
        return userService.findAllUsers();
    }

    @Override
    @WebMethod
    public boolean containsUser(@Nullable final String login) throws Exception {
        return userService.contains(login);
    }

    @Override
    @WebMethod
    public boolean isEmptyUserRepository(@Nullable final Session userSession) throws Exception {
        validate(userSession);
        return userService.isEmptyRepository();
    }

}
