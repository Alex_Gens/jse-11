package ru.kazakov.iteco.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.endpoint.ITaskEndpoint;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.entity.Session;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.enumeration.SortType;
import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.kazakov.iteco.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    private final ITaskService taskService;

    public TaskEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
        this.taskService = serviceLocator.getTaskService();
    }

    @Override
    @WebMethod
    public void mergeTask(@Nullable final Session userSession, @Nullable final Task entity) throws Exception {
        validate(userSession);
        taskService.merge(entity);
    }

    @Override
    @WebMethod
    public void persistTask(@Nullable final Session userSession, @Nullable Task entity) throws Exception {
        validate(userSession);
        taskService.persist(entity);
    }

    @Override
    @WebMethod
    public void removeTaskById(@Nullable final Session userSession, @Nullable final String id) throws Exception {
        validate(userSession);
        taskService.remove(id);
    }

    @Override
    @WebMethod
    public void removeTasksByIds(@Nullable final Session userSession, @Nullable final List<String> ids) throws Exception {
        validate(userSession);
        taskService.remove(ids);
    }

    @Override
    @WebMethod
    public void removeTasksWithProject(@Nullable final Session userSession, @Nullable final String projectId) throws Exception {
        validate(userSession);
        taskService.removeWithProject(userSession.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public void removeAllTasks(@Nullable final Session userSession) throws Exception {
        validate(userSession);
        taskService.removeAll();
    }

    @Override
    @WebMethod
    public void removeAllTasksByCurrentId(@Nullable final Session userSession) throws Exception {
        validate(userSession);
        taskService.removeAll(userSession.getUserId());
    }

    @Override
    @WebMethod
    public void removeAllTasksWithProjects(@Nullable final Session userSession) throws Exception {
        validate(userSession);
        taskService.removeAllWithProjects(userSession.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public Task findByTaskName(@Nullable final Session userSession, @Nullable final String name) throws Exception {
        validate(userSession);
        return taskService.findByName(name);
    }

    @Nullable
    @Override
    @WebMethod
    public Task findByTaskNameCurrentId(@Nullable final Session userSession, @Nullable final String name) throws Exception {
        validate(userSession);
        return taskService.findByName(userSession.getUserId(), name);
    }

    @Nullable
    @Override
    @WebMethod
    public Task findOneTask(@Nullable final Session userSession, @Nullable final String id) throws Exception {
        validate(userSession);
        return taskService.findOne(id);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Task> findAllTasks(@Nullable final Session userSession) throws Exception {
        validate(userSession);
        return taskService.findAll();
    }

    @Nullable
    @Override
    @WebMethod
    public List<String> findAllSortedTasksByCurrentIdProjectId(@Nullable final Session userSession, @Nullable final String projectId, @Nullable SortType sortType) throws Exception {
        validate(userSession);
        return taskService.findAll(userSession.getUserId(), projectId, sortType);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Task> findAllTasksByIds(@Nullable final Session userSession, @Nullable final List<String> ids) throws Exception {
        validate(userSession);
        return taskService.findAll(ids);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Task> findAllTasksByCurrentId(@Nullable final Session userSession) throws Exception {
        validate(userSession);
        return taskService.findAll(userSession.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public List<String> findAllSortedTasksByCurrentId(@Nullable final Session userSession, @Nullable final SortType sortType) throws Exception {
        validate(userSession);
        return taskService.findAll(userSession.getUserId(), sortType);
    }

    @Nullable
    @Override
    @WebMethod
    public List<String> findAllTasksByNameCurrentId(@Nullable final Session userSession, @Nullable final String part) throws Exception {
        validate(userSession);
        return taskService.findAllByName(userSession.getUserId(), part);
    }

    @Nullable
    @Override
    @WebMethod
    public List<String> findAllTasksByInfoCurrentId(@Nullable final Session userSession, @Nullable final String part) throws Exception {
        validate(userSession);
        return taskService.findAllByInfo(userSession.getUserId(), part);
    }

    @Override
    @WebMethod
    public boolean containsTask(@Nullable final Session userSession, @Nullable final String name) throws Exception {
        validate(userSession);
        return taskService.contains(name);
    }

    @Override
    @WebMethod
    public boolean containsTaskByCurrentId(@Nullable final Session userSession, @Nullable final String name) throws Exception {
        validate(userSession);
        return taskService.contains(userSession.getUserId(), name);
    }

    @Override
    @WebMethod
    public boolean isEmptyTask(@Nullable final Session userSession, @Nullable final String id) throws Exception {
        validate(userSession);
        return taskService.isEmpty(id);
    }

    @Override
    @WebMethod
    public boolean isEmptyTaskRepository(@Nullable final Session userSession) throws Exception {
        validate(userSession);
        return taskService.isEmptyRepository();
    }

    @Override
    @WebMethod
    public boolean isEmptyTaskRepositoryByCurrentId(@Nullable final Session userSession) throws Exception {
        validate(userSession);
        return taskService.isEmptyRepository(userSession.getUserId());
    }

}
