package ru.kazakov.iteco.endpoint;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.service.ISessionService;
import ru.kazakov.iteco.constant.Constant;
import ru.kazakov.iteco.entity.Session;
import ru.kazakov.iteco.util.SignatureUtil;

@RequiredArgsConstructor
public abstract class AbstractEndpoint {

    @NotNull
    protected final ServiceLocator serviceLocator;

    protected void validate(@Nullable final Session userSession) throws Exception {
        if(userSession == null) throw new Exception();
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        if (!sessionService.contains(userSession.getUserId(), userSession.getId())) throw new Exception();
        @Nullable final Session session = sessionService.findOne(userSession.getId());
        if (session == null) throw new Exception();
        if (session.getSignature() == null) throw new Exception();
        userSession.setSignature(null);
        if (!session.getSignature().equals(SignatureUtil.getSignature(userSession))) throw new Exception("Session is invalid");
        if (System.currentTimeMillis() - userSession.getTimestamp() > Constant.SESSION_TIME) throw new Exception("Session time out\n you need to log in again");
    }

}
