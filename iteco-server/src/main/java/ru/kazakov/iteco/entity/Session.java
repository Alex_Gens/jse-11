package ru.kazakov.iteco.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.enumeration.RoleType;

@Getter
@Setter
@NoArgsConstructor
public final class Session extends AbstractEntity {

    @NotNull
    private String userId;

    @NotNull
    private RoleType roleType;

    private long timestamp = System.currentTimeMillis();

    @Nullable
    private  String signature;

}
