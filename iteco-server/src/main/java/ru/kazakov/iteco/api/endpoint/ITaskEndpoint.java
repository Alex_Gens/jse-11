package ru.kazakov.iteco.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.Session;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.enumeration.SortType;
import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @WebMethod
    public void mergeTask(@Nullable final Session userSession,
                          @Nullable final Task entity) throws Exception;

    @WebMethod
    public void persistTask(@Nullable final Session userSession,
                            @Nullable final Task entity) throws Exception;

    @WebMethod
    public void removeTaskById(@Nullable final Session userSession,
                               @Nullable final String id) throws Exception;

    @WebMethod
    public void removeTasksByIds(@Nullable final Session userSession,
                                 @Nullable final List<String> ids) throws Exception;

    @WebMethod
    public void removeTasksWithProject(@Nullable final Session userSession,
                                       @Nullable final String projectId) throws Exception;

    @WebMethod
    public void removeAllTasks(@Nullable final Session userSession) throws Exception;

    @WebMethod
    public void removeAllTasksByCurrentId(@Nullable final Session userSession) throws Exception;

    @WebMethod
    public void removeAllTasksWithProjects(@Nullable final Session userSession) throws Exception;

    @Nullable
    @WebMethod
    public Task findByTaskName(@Nullable final Session userSession,
                               @Nullable final String name) throws Exception;

    @Nullable
    @WebMethod
    public Task findByTaskNameCurrentId(@Nullable final Session userSession,
                                        @Nullable final String name) throws Exception;

    @Nullable
    @WebMethod
    public Task findOneTask(@Nullable final Session userSession,
                            @Nullable final String id) throws Exception;

    @Nullable
    @WebMethod
    public List<Task> findAllTasks(@Nullable final Session userSession) throws Exception;

    @Nullable
    @WebMethod
    public List<String> findAllSortedTasksByCurrentIdProjectId(@Nullable final Session userSession,
                                                               @Nullable final String projectId,
                                                               @Nullable final SortType sortType) throws Exception;

    @Nullable
    @WebMethod
    public List<Task> findAllTasksByIds(@Nullable final Session userSession,
                                        @Nullable final List<String> ids) throws Exception;

    @Nullable
    @WebMethod
    public List<Task> findAllTasksByCurrentId(@Nullable final Session userSession) throws Exception;

    @Nullable
    @WebMethod
    public List<String> findAllSortedTasksByCurrentId(@Nullable final Session userSession,
                                                      @Nullable final SortType sortType) throws Exception;
    @Nullable
    @WebMethod
    public List<String> findAllTasksByNameCurrentId(@Nullable final Session userSession,
                                                    @Nullable final String part) throws Exception;
    @Nullable
    @WebMethod
    public List<String> findAllTasksByInfoCurrentId(@Nullable final Session userSession,
                                                    @Nullable final String part) throws Exception;

    @WebMethod
    public boolean containsTask(@Nullable final Session userSession,
                                @Nullable final String name) throws Exception;

    @WebMethod
    public boolean containsTaskByCurrentId(@Nullable final Session userSession,
                                           @Nullable final String name) throws Exception;

    @WebMethod
    public boolean isEmptyTask(@Nullable final Session userSession,
                               @Nullable final String id) throws Exception;

    @WebMethod
    public boolean isEmptyTaskRepository(@Nullable final Session userSession) throws Exception;

    @WebMethod
    public boolean isEmptyTaskRepositoryByCurrentId(@Nullable final Session userSession) throws Exception;

}
