package ru.kazakov.iteco.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    public String getHost();

    @NotNull
    public String getPort();

    @NotNull
    public String getSalt();

    @NotNull
    public String getCycle();

    @NotNull
    public String getSessionTime();

}
