package ru.kazakov.iteco.api.service;

import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.User;
import java.util.List;


public interface IUserService extends IService<User> {

    @Nullable
    public String getName(@Nullable final String id) throws Exception;

    @Nullable
    public User findByLogin(@Nullable final String login) throws Exception;

    @Nullable
    public List<String> findAllUsers();

    public boolean contains(@Nullable final String login) throws Exception;

}
