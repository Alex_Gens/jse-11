package ru.kazakov.iteco;

import ru.kazakov.iteco.context.Bootstrap;
import java.io.IOException;

public final class Application {

    public static void main(String[] args) {new Bootstrap().start();}

}
