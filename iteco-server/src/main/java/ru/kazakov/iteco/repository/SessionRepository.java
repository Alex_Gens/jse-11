package ru.kazakov.iteco.repository;

import org.jetbrains.annotations.NotNull;
import ru.kazakov.iteco.api.repository.ISessionRepository;
import ru.kazakov.iteco.entity.Session;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    public boolean contains(@NotNull final String userId, @NotNull final String id) {
        return entities.values().stream().anyMatch(v -> v.getUserId().equals(userId) &&
                                                        v.getId().equals(id));
    }

}
