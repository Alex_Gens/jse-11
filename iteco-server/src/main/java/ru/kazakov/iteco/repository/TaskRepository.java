package ru.kazakov.iteco.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.repository.ITaskRepository;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.enumeration.SortType;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Nullable
    @Override
    public String getName(@NotNull final String id) {return entities.get(id).getName();}

    @Nullable
    @Override
    public String getProjectId(@NotNull final String id) {return entities.get(id).getProjectId();}

    @Override
    public void setProjectId(@NotNull final String id, @NotNull final String projectId) {entities.get(id).setProjectId(projectId);}

    @Override
    public void merge(@NotNull final Task entity) {
        entities.merge(entity.getId(), entity, (v1, v2)
                -> {if (v2.getDateStart() != null) v1.setDateStart(v2.getDateStart());
                    if (v2.getDateFinish() != null) v1.setDateFinish(v2.getDateFinish());
                    if (v2.getStatus() != null) v1.setStatus(v2.getStatus());
                    if (v2.getInfo() != null && !v2.getInfo().isEmpty()) v1.setInfo(v2.getInfo());
                    if (v2.getProjectId() != null && !v2.getProjectId().isEmpty()) v1.setProjectId(v2.getProjectId());
                return v1;});
    }

    @Override
    public void remove(@NotNull final List<String> ids) {
        entities.entrySet().removeIf(entry
                -> ids.contains(entry.getValue().getId()));
    }

    @Override
    public void removeWithProject(@NotNull final String currentUserId, @NotNull final String projectId) {
        entities.entrySet().removeIf(entry -> entry.getValue().getUserId().equals(currentUserId) &&
                (entry.getValue().getProjectId() != null && entry.getValue().getProjectId().equals(projectId)));
    }

    @Override
    public void removeAll(@NotNull final String currentUserId) {
        entities.entrySet().removeIf(entry -> entry.getValue().getUserId().equals(currentUserId));
    }

    @Override
    public void removeAllWithProjects(@NotNull final String currentUserId) {
        entities.entrySet().removeIf(entry -> entry.getValue().getUserId().equals(currentUserId) &&
                (entry.getValue().getProjectId() != null && !entry.getValue().getProjectId().isEmpty()));
    }

    @Nullable
    @Override
    public Task findByName(@NotNull final String name) {
        return entities.values().stream()
                .filter(v -> v.getName().equals(name))
                .findFirst().orElse(null);
    }

    @Nullable
    @Override
    public Task findByName(@NotNull final String name, @NotNull final String currentUserId) {
        return entities.values().stream()
                .filter(v -> v.getName().equals(name) && v.getUserId().equals(currentUserId))
                .findFirst().orElse(null);
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final List<String> ids) {
        return entities.values().stream()
                .filter( v -> ids.contains(v.getId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String currentUserId) {
        return  entities.values().stream()
                .filter( v -> v.getUserId().equals(currentUserId))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<String> findAll(@NotNull final String currentUserId, @NotNull final SortType sortType) {
        @NotNull List <Task> tasks = entities.values().stream()
                .filter( v -> v.getUserId().equals(currentUserId))
                .collect(Collectors.toList());
        switch (sortType) {
            case START : tasks.sort(Comparator.nullsLast(Comparator.comparing(Task::getDateStart))); break;
            case FINISH: tasks.sort(Comparator.nullsLast(Comparator.comparing(Task::getDateFinish))); break;
            case STATUS: tasks.sort(Comparator.comparing(Task::getStatus)); break;
        }
        return tasks.stream().map(Task::getName).collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<String> findAll(@NotNull final String currentUserId, @NotNull final String projectId,
                                @NotNull final SortType sortType) {
        List<Task> tasks = entities.values().stream()
                .filter( v -> v.getUserId().equals(currentUserId) &&
                             (v.getProjectId() != null && v.getProjectId().equals(projectId)))
                .collect(Collectors.toList());
        switch (sortType) {
            case START : tasks.sort(Comparator.nullsLast(Comparator.comparing(Task::getDateStart))); break;
            case FINISH: tasks.sort(Comparator.nullsLast(Comparator.comparing(Task::getDateFinish))); break;
            case STATUS: tasks.sort(Comparator.comparing(Task::getStatus)); break;
        }
        return tasks.stream().map(Task::getName).collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<String> findAllByName(@NotNull final String currentUserId, @NotNull final String part) {
        return entities.values().stream()
                .filter(v -> v.getUserId().equals(currentUserId) &&
                        v.getName().toLowerCase().contains(part.toLowerCase().trim()))
                .map(Task::getName).collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<String> findAllByInfo(@NotNull final String currentUserId, @NotNull final String part) {
        return entities.values().stream()
                .filter(v -> v.getUserId().equals(currentUserId) &&
                        v.getInfo().toLowerCase().contains(part.toLowerCase().trim()))
                .map(Task::getName).collect(Collectors.toList());
    }

    @Override
    public boolean contains(@NotNull final String name) {
        return entities.values().stream()
                .anyMatch(v -> v.getName().equals(name));
    }

    @Override
    public boolean contains(@NotNull final String currentUserId, @NotNull final String name) {
        return entities.values().stream()
                .anyMatch(v -> v.getName().equals(name) && v.getUserId().equals(currentUserId));
    }

    @Override
    public boolean isEmpty(@NotNull final String id) {return entities.get(id).isEmpty();}

    @Override
    public boolean isEmptyRepository(@NotNull final String currentUserId) {
        return entities.values().stream()
                .noneMatch(v -> v.getUserId().equals(currentUserId));
    }

}
